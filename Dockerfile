FROM alpine:3.18.4

RUN apk -U add py3-pip \
    && pip install pytest tftest \
    && wget -q https://github.com/opentofu/opentofu/releases/download/v1.6.1/tofu_1.6.1_linux_amd64.zip -O /tmp/tofu.zip \
    && unzip /tmp/tofu.zip tofu -d /usr/local/bin/ \
    && wget -q https://github.com/gruntwork-io/terragrunt/releases/download/v0.55.3/terragrunt_linux_amd64 -O /usr/local/bin/terragrunt \
    && chmod +x /usr/local/bin/terragrunt \
    && wget -q https://github.com/terraform-linters/tflint/releases/download/v0.50.3/tflint_linux_amd64.zip -O /tmp/tflint.zip \
    && unzip /tmp/tflint.zip -d /usr/local/bin/ \
    && rm -f /tmp/tflint.zip /var/cache/apk/*

ENTRYPOINT [""]
